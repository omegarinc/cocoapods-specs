#
# Be sure to run `pod lib lint ORReachabilityHelper.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ORReachabilityHelper"
  s.version          = "0.1.0"
  s.summary          = "Check Internet reachability."

  s.description      = "Use when you want to find out if the Internet is availiable."

  s.homepage         = "https://bitbucket.org/omegarinc/reachability-helper"
  s.license          = 'MIT'
  s.author           = { "Alexander Kurbanov" => "kurbanov.alex@omega-r.com" }
  s.source           = { :git => "https://bitbucket.org/omegarinc/reachability-helper", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'ORReachabilityHelper' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'Foundation'
end
