#
# Be sure to run `pod lib lint ORTextFieldEx.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ORTextFieldEx"
  s.version          = "1.0"
  s.summary          = "Extended UITextField"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
Extended UITextField with validation, formating, messages and autocomplete
                       DESC

  s.homepage         = "https://bitbucket.org/omegarinc/ortextfieldex.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
s.author           = { "Alexander Lobanov" => "alexander.lobanov@omega-r.com", "Alexander Kurbanov" => "kurbanov.alex@omega-r.com", "Maxim Solovyev" => "maxim@omega-r.com" }
  s.source           = { :git => "https://bitbucket.org/omegarinc/ortextfieldex.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resources = 'Pod/Assets/**/*'

#  s.resource_bundles = {
#    'images' => ['Pod/Assets/**/*']
#  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
