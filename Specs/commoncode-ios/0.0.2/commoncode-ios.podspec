#
#  Be sure to run `pod spec lint commoncode-ios.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "commoncode-ios"
  s.version      = "0.0.2"
  s.summary      = "Common code ios"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description  = "Common code for ios applications"
  s.homepage     = "https://bitbucket.org/omegarinc/commoncode-ios"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See http://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  # s.author             = { "FIO" => "EMAIL" }
  # Or just: s.author    = "FIO"
  s.authors            = { "Maxim Soloviev" => "maxim@omega-r.com", "Alexander Kurbanov" => "kurbanov.alex@omega-r.com" }
  # s.social_media_url   = "http://twitter.com/account"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # s.platform     = :ios
  s.platform     = :ios, "7.0"

  #  When using multiple platforms
  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"
  # s.watchos.deployment_target = "2.0"
  # s.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  s.source       = { :git => "https://bitbucket.org/omegarinc/commoncode-ios.git", :tag => s.version.to_s }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  s.source_files  = "Classes/**/*.{h,m}"
  # s.exclude_files = "Classes/Exclude"

  s.public_header_files = "Classes/*.h", "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  s.framework  = "UIKit"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

  # -- SubSpecs Definition ―――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  
	s.subspec 'BundleHelper' do |bundleHelper|
    	bundleHelper.source_files = 'Classes/BundleHelper.{h,m}'
	end

	s.subspec 'CommonHelpers' do |commonHelpers|
    	commonHelpers.source_files = 'Classes/CommonHelpers.{h,m}'
	end

	s.subspec 'FileHelper' do |fileHelper|
    	fileHelper.source_files = 'Classes/FileHelper.{h,m}'
	end

	s.subspec 'GraphicHelpers' do |graphicHelpers|
    	graphicHelpers.source_files = 'Classes/GraphicHelpers.{h,m}'
	end

	s.subspec 'Macro' do |macro|
    	macro.source_files = 'Classes/Macro.{h,m}'
	end

	s.subspec 'NSArray+Extended' do |nsArrayExtended|
    	nsArrayExtended.source_files = 'Classes/NSArray+Extended.{h,m}'
		nsArrayExtended.dependency 'commoncode-ios/Macro'
	end

	s.subspec 'NSDate+Extended' do |nsDateExtended|
    	nsDateExtended.source_files = 'Classes/NSDate+Extended.{h,m}'
	end

	s.subspec 'NSString+EmailValidator' do |nsStringEmailValidator|
    	nsStringEmailValidator.source_files = 'Classes/NSString+EmailValidator.{h,m}'
	end

	s.subspec 'NSString+Extended' do |nsStringExtended|
    	nsStringExtended.source_files = 'Classes/NSString+Extended.{h,m}'
	end

	s.subspec 'NSString+Youtube' do |nsStringYoutube|
    	nsStringYoutube.source_files = 'Classes/NSString+Youtube.{h,m}'
		nsStringYoutube.dependency 'commoncode-ios/NSString+Extended'
	end

	s.subspec 'UIImage+ImageWithAlpha' do |uiImageImageWithAlpha|
    	uiImageImageWithAlpha.source_files = 'Classes/UIImage+ImageWithAlpha.{h,m}'
	end

	s.subspec 'UINavigationController+Fade' do |uiNavigationControllerFade|
    	uiNavigationControllerFade.source_files = 'Classes/UINavigationController+Fade.{h,m}'
	end

	s.subspec 'UserDefaultsManager' do |userDefaultsManager|
    	userDefaultsManager.source_files = 'Classes/UserDefaultsManager.{h,m}'
	end
	
	#
	# views
	#
	
	s.subspec 'BarButtonWithImageAndTitleFactory' do |barButtonWithImageAndTitleFactory|
    	barButtonWithImageAndTitleFactory.source_files = 'Classes/Views/BarButtonWithImageAndTitleFactory.{h,m}'
		barButtonWithImageAndTitleFactory.dependency 'commoncode-ios/UIImage+ImageWithAlpha'
	end

	s.subspec 'HorizontalSeparator' do |horizontalSeparator|
    	horizontalSeparator.source_files = 'Classes/Views/HorizontalSeparator.{h,m}'
	end

	s.subspec 'TopAlignedLabel' do |topAlignedLabel|
    	topAlignedLabel.source_files = 'Classes/Views/TopAlignedLabel.{h,m}'
	end

	s.subspec 'UIGroup' do |uiGroup|
    	uiGroup.source_files = 'Classes/Views/UIGroup.{h,m}'
	end

	s.subspec 'UIImageView+MediaThumbnail' do |uiImageViewMediaThumbnail|
    	uiImageViewMediaThumbnail.source_files = 'Classes/Views/UIImageView+MediaThumbnail.{h,m}'
		uiImageViewMediaThumbnail.frameworks = 'AVFoundation'
	end

	s.subspec 'UIScrollView+DisableDelaysContentTouches' do |uiScrollViewDisableDelaysContentTouches|
    	uiScrollViewDisableDelaysContentTouches.source_files = 'Classes/Views/UIScrollView+DisableDelaysContentTouches.{h,m}'
	end

	s.subspec 'UITextView+Extended' do |uiTextViewExtended|
    	uiTextViewExtended.source_files = 'Classes/Views/UITextView+Extended.{h,m}'
	end

	s.subspec 'UIView+Extended' do |uiViewExtended|
    	uiViewExtended.source_files = 'Classes/Views/UIView+Extended.{h,m}'
	end
	
	s.subspec 'UIView+IBDesignable' do |uiViewIBDesignable|
    	uiViewIBDesignable.source_files = 'Classes/Views/UIView+IBDesignable.{h,m}'
	end
	
	s.subspec 'UIView+Mask' do |uiViewMask|
    	uiViewMask.source_files = 'Classes/Views/UIView+Mask.{h,m}'
	end

end
