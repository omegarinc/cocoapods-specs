#
# Be sure to run `pod lib lint Redshift.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'Redshift'
    s.version          = '1.0.0'
    s.summary          = 'Redshift 360 Video Player SDK'

    s.description      = <<-DESC
    A fully featured 360 Video Player SDK for iOS.
    DESC

    s.homepage         = 'https://bitbucket.org/omegarinc/orredshift'
    s.license          = 'Proprietary'
    s.author           = { 'Kent Karlsson' => 'kent@stonevalleypartners.com' }
    s.source           = { :git => "https://bitbucket.org/omegarinc/orredshift.git", :tag => s.version }

    s.ios.deployment_target = '8.0'

    s.source_files = 'Redshift/Classes/**/*'

    s.resource_bundles = {
      'Redshift' => [
        'Redshift/Assets/*.xcassets',
        'Redshift/Assets/*.xib'
      ]
    }

    s.dependency 'ORGoogleVR'
end

