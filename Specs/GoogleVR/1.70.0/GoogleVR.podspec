Pod::Spec.new do |s|
    s.name             = 'GoogleVR'
    s.version          = '1.70.0'
    s.authors          = 'Google, Inc.'
    s.homepage         = 'https://developers.google.com/vr/'
    s.summary          = 'Google VR SDK for iOS developers.'
    s.license          = { :text => "Copyright 2016 Google Inc.", :type => "Copyright" }
    s.source           = { :http => "https://raw.githubusercontent.com/qaasx/test/master/googlevr-#{s.version.to_s}.tgz"}

    s.ios.deployment_target = '8.0'

    s.frameworks = 'AudioToolbox', 'AVFoundation', 'CoreGraphics', 'CoreMedia', 'CoreMotion', 'CoreText', 'CoreVideo', 'GLKit', 'MediaPlayer', 'OpenGLES', 'QuartzCore'
    s.library = 'c++'

    s.pod_target_xcconfig = {
        'OTHER_LDFLAGS' => '$(inherited) -lObjC'
    }

    s.vendored_framework  = 'Frameworks/GoogleVR.framework'
    s.preserve_paths      = '**/*'

    s.pod_target_xcconfig = {
        'FRAMEWORK_SEARCH_PATHS' => '${PODS_ROOT}/GoogleVR'
    }

    s.dependency 'GTMSessionFetcher/Core', '~> 1.1'
    s.dependency 'GoogleToolboxForMac/Logger', '~> 2.1'
end