#
# Be sure to run `pod lib lint ORCropImageController.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ORCropImageController"
  s.version          = "0.1.3"
  s.summary          = "ORCropImageController allows user to perform crop of the image."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = "https://bitbucket.org/omegarinc/orcropimagecontroller"
  # s.screenshots     = ""
  s.license          = 'MIT'
  s.author           = { "Nikita Egoshin" => "nikita.egoshin@omega-r.com" }
  s.source           = { :git => "https://NikitaEgoshin@bitbucket.org/omegarinc/orcropimagecontroller.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'ORCropImageController/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ORCropImageController' => ['ORCropImageController/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit'
end
