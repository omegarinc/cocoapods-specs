#
# Be sure to run `pod lib lint ORImageGallery.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ORGoogleVR'
  s.version          = '1.7.0'
  s.summary          = 'ORGoogleVR - use GVRSDK on swift projects'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "ORGoogleVR - framework for use GVRSDK on swift pods or projects"

  s.homepage         = 'https://bitbucket.org/omegarinc/orgooglevr'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Evgeny Ivanov' => 'evgeny.ivanov@omega-r.com' }
  s.source           = { :git => "https://bitbucket.org/omegarinc/orgooglevr.git", :tag => s.version.to_s }
  
  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = 'GoogleVR.framework'

end
