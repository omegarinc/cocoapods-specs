#
# Be sure to run `pod lib lint ORPomogator.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ORPomogator'
  s.version          = '0.1.2'
  s.summary          = 'ORPomogator - helping pod for main Pomogator application.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What s the focus?
#   * Try to keep it short, snappy and to the point.i
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "Data structure, popup"

  s.homepage         = 'https://bitbucket.org/omegarinc/orpomogator'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dmitriy Mamatov' => 'dmitriy.mamatov@omega-r.com' }
  s.source           = { :git => 'https://bitbucket.org/omegarinc/orpomogator.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.requires_arc = true
  s.ios.deployment_target = '8.0'

  s.source_files = 'ORPomogator/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ORPomogator' => ['ORPomogator/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
